type Gender = 'male' | 'female' | 'others'
import type { Role } from './Role'
type User = {
  id?: number
  email: string
  password: string
  fullName: string
  gender: Gender // Male, Female, Others
  roles: Role[] // admin, user
}

export type { Gender, User }
